import './App.css';
import Banner from './components/Banner';
import AppNavbar from './components/AppNavBar';
import Highlights from './components/Highlights'



export default function App() {
  return (
      <div>
         <AppNavbar />
         <Highlights />  
         <Banner />  
     </div>
    );
}