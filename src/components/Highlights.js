import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';


export default function Highlights(){
	return(
      <Row className="mt-5 mb-5">
   		  <Col xs={12} md={4}>
   		      {/*1st Card Component*/}
      		  <Card className="p-3">
      		  	  <Card.Body>
      		  	  	 <Card.Title>Learn From Home</Card.Title>
      		  	  	 <Card.Text>
      		  	  	 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci maiores quasi similique aliquam sint amet!
      		  	  	 </Card.Text>
      		  	  </Card.Body>
      		  </Card>	
   		  </Col>

   		  <Col xs={12} md={4}>
      		
      		  <Card className="p-3">
      		  	  <Card.Body>
      		  	  	 <Card.Title>Learn From Home</Card.Title>
      		  	  	 <Card.Text>
      		  	  	 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci maiores quasi similique aliquam sint amet!
      		  	  	 </Card.Text>
      		  	  </Card.Body>
      		  </Card>
   		  </Col>

   		  <Col xs={12} md={4}>
      		  {/*3rd Card Component*/}	
      		  <Card className="p-3">
      		  	  <Card.Body>
      		  	  	 <Card.Title>Learn From Home</Card.Title>
      		  	  	 <Card.Text>
      		  	  	 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci maiores quasi similique aliquam sint amet!
      		  	  	 </Card.Text>
      		  	  </Card.Body>
      		  </Card>
   		  </Col>
      </Row>
	);
};


